use std::fs;

fn main() {
    let input = fs::read_to_string("./inputs/day1").unwrap();
    let part_one_result = calories_of_elf_carrying_the_most_calories(&input);
    println!("Part One: {}", part_one_result);

    let part_two_result = calories_of_top_three_elves_carrying_the_most_calories(&input);
    println!("Part Two: {}", part_two_result);
}

fn calories_of_elf_carrying_the_most_calories(input: &str) -> i64 {
    let all_calories = elfs_calories(input);

    *all_calories.iter().max().unwrap()
}

fn calories_of_top_three_elves_carrying_the_most_calories(input: &str) -> i64 {
    let mut all_calories = elfs_calories(input);

    all_calories.sort();
    all_calories.reverse();

    all_calories.iter().take(3).sum()
}

fn elfs_calories(input: &str) -> Vec<i64> {
    let input = input
        .split('\n')
        .map(|s| if s.is_empty() { None } else { Some(s.parse::<i64>().unwrap()) });

    let mut all_calories = vec![0];

    for line in input {
        if let Some(calories) = line {
            if let Some(current_calories) = all_calories.last_mut() {
                *current_calories += calories;
            };
        } else {
            all_calories.push(0);
        }
    }
    all_calories
}

#[cfg(test)]
mod tests {
    use super::*;
    const INPUT: &str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

    #[test]
    fn test_part_one_sample() {
        let result = calories_of_elf_carrying_the_most_calories(INPUT);
        assert_eq!(result, 24000);
    }

    #[test]
    fn test_part_two_sample() {
        let result = calories_of_top_three_elves_carrying_the_most_calories(INPUT);
        assert_eq!(result, 45000);
    }
}